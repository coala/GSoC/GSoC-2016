## Welcome to the coala GSoC organizing repository

### Admins
If you have any problem or concern you feel you can not address to your
student/mentor __please__ do contact one of our GSoC Admins:

Max Scholz: m0hahawk[at]gmail.com

Lasse Schuirmann: lasse.schuirmann[at]gmail.com


### Tasks
#### Due to 2016.05.08
###### Students:
- Make sure you have access to https://gitlab.com/coala/GSoC2016/milestones
- Add a milestone for your project. Set the date to Aug 23, 2016.
- Add issues for all your project milestones from your schedule. Be sure to set due dates to the issues, have the issue associated with your milestone and assigned to you!
- Get active! Among other things you can: Blogging about coala, Help us manage coala
- Make a mockup and some further planning

Please note that Google requires us to fail you if you do not get active during community bonding phase so do not feel like bonding is optional.

###### Mentors:
- Make sure your students do all of the above!
- Set up a weekly meeting with your student from now on.
